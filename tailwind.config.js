/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      animation: {
        tilt: 'tilt 14s infinite ease-in-out',
        "move-about": 'move-about 20s infinite ease-in-out',
      },
      keyframes: {
        tilt: {
          "0%, 100%": {
            transform: "rotate(1deg)",
            filter: "hue-rotate(0deg) blur(8px)",
          },
          "50%": {
            transform: "rotate(-1deg)",
            filter: "hue-rotate(20deg) blur(8px)",
          },
        },
        "move-about": {
          "0%, 100%": {
            transform: "translate(0) scale(1)",
          },
          "50%": {
            transform: "translate(20px, 8px) scale(1.4)",
          },
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
