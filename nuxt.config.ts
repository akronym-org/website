// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  telemetry: false,
  ssr: false,
  nitro: {
    preset: 'cloudflare_pages',
  },
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxt/content',
    'nuxt-icon',
  ],
  app: {
    head: {
      htmlAttrs: {
        lang: 'de'
      },
      title: 'software für sozialen wandel | akronym',
      meta: [
        { name: 'description', content: 'Wir bauen und skalieren Apps für sozialen Fortschritt.' }
      ],
    }
  },
  pageTransition: {'page': true }
})
