# Nuxt 3 Minimal Starter

Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) to learn more.

## Setup

Make sure to install the dependencies: `yarn install`

## Development Server

Start the development server on http://localhost:3000: `yarn dev`

## More

Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.
