[<< Start](/)

<!--sse--> 
Wir informieren Sie nachfolgend gemäß den gesetzlichen Vorgaben des Datenschutzrechts (insb. gemäß BDSG n.F. und der europäischen Datenschutz-Grundverordnung ‚DS-GVO‘) über die Art, den Umfang und Zweck der Verarbeitung personenbezogener Daten durch unser Unternehmen. Diese Datenschutzerklärung gilt auch für unsere Websites und Sozial-Media-Profile. Bezüglich der Definition von Begriffen wie etwa „personenbezogene Daten“ oder „Verarbeitung“ verweisen wir auf Art. 4 DS-GVO.
Name und Kontaktdaten des / der Verantwortlichen
Unser/e Verantwortliche/r (nachfolgend „Verantwortlicher“) i.S.d. Art. 4 Zif. 7 DS-GVO ist:

Valentin Beyer\
akronym\
12051 Berlin\
E-Mail-Adresse: info@akronym.org

Datenarten, Zwecke der Verarbeitung und Kategorien betroffener Personen
Nachfolgend informieren wir Sie über Art, Umfang und Zweck der Erhebung, Verarbeitung und Nutzung personenbezogener Daten.
1. Arten der Daten, die wir verarbeiten
Nutzungsdaten (Zugriffszeiten, besuchte Websites etc.), Bestandsdaten (Name, Adresse etc.), Kontaktdaten (Telefonnummer, E-Mail, Fax etc.), Zahlungsdaten (Bankdaten, Kontodaten, Zahlungshistorie etc.), Vertragsdaten (Gegenstand des Vertrages, Laufzeit etc.), Inhaltsdaten (Texteingaben, Videos, Fotos etc.), Kommunikationsdaten (IP-Adresse etc.), 

2. Zwecke der Verarbeitung nach Art. 13 Abs. 1 c) DS-GVO
Abwicklung von Verträgen, Beweiszwecke / Beweissicherung, Website technisch und wirtschaftlich optimieren, Leichten Zugang zur Website ermöglichen, Erfüllung vertraglicher Verpflichtungen, Kontaktaufnahme bei juristischer Beanstandung durch Dritte, Erfüllung gesetzlicher Aufbewahrungspflichten, Optimierung und statistische Auswertung unserer Dienste, Kommerzielle Nutzung der Website unterstützen, Nutzererfahrung verbessern, Website benutzerfreundlich gestalten, Wirtschaftlicher Betrieb der Werbung und Website, Marketing / Vertrieb / Werbung, Erstellung von Statistiken,Kopierwahrscheinlichkeit von Texten ermitteln, Vermeidung von SPAM und Missbrauch, Abwicklung eines Bewerberverfahrens, Kundenservice und Kundenpflege, Kontaktanfragen abwickeln, Websites mit Funktionen und Inhalten bereitstellen, Maßnahmen der Sicherheit, Unterbrechungsfreier,sicherer Betrieb unserer Website, 

3. Kategorien der betroffenen Personen nach Art. 13 Abs. 1 e) DS-GVO
Besucher/Nutzer der Website, Kunden, Lieferanten, Interessenten, Bewerber, Beschäftigte, Beschäftigte von Kunden oder Lieferanten, 

Die betroffenen Personen werden zusammenfassend als „Nutzer“ bezeichnet.


Rechtsgrundlagen der Verarbeitung personenbezogener Daten
Nachfolgend Informieren wir Sie über die Rechtsgrundlagen der Verarbeitung personenbezogener Daten:

Wenn wir Ihre Einwilligung für die Verarbeitung personenbezogenen Daten eingeholt haben, ist Art. 6 Abs. 1 S. 1 lit. a) DS-GVO Rechtsgrundlage.
Ist die Verarbeitung zur Erfüllung eines Vertrags oder zur Durchführung vorvertraglicher Maßnahmen erforderlich, die auf Ihre Anfrage hin erfolgen, so ist Art. 6 Abs. 1 S. 1 lit. b) DS-GVO Rechtsgrundlage.
Ist die Verarbeitung zur Erfüllung einer rechtlichen Verpflichtung erforderlich, der wir unterliegen (z.B. gesetzliche Aufbewahrungspflichten), so ist Art. 6 Abs. 1 S. 1 lit. c) DS-GVO Rechtsgrundlage.
Ist die Verarbeitung erforderlich, um lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person zu schützen, so ist Art. 6 Abs. 1 S. 1 lit. d) DS-GVO Rechtsgrundlage.
Ist die Verarbeitung zur Wahrung unserer oder der berechtigten Interessen eines Dritten erforderlich und überwiegen diesbezüglich Ihre Interessen oder Grundrechte und Grundfreiheiten nicht, so ist Art. 6 Abs. 1 S. 1 lit. f) DS-GVO Rechtsgrundlage.


Weitergabe personenbezogener Daten an Dritte und Auftragsverarbeiter
Ohne Ihre Einwilligung geben wir grundsätzlich keine Daten an Dritte weiter. Sollte dies doch der Fall sein, dann erfolgt die Weitergabe auf der Grundlage der zuvor genannten Rechtsgrundlagen z.B. bei der Weitergabe von Daten an Online-Paymentanbieter zur Vertragserfüllung oder aufgrund gerichtlicher Anordnung oder wegen einer gesetzlichen Verpflichtung zur Herausgabe der Daten zum Zwecke der Strafverfolgung, zur Gefahrenabwehr oder zur Durchsetzung der Rechte am geistigen Eigentum.
Wir setzen zudem Auftragsverarbeiter (externe Dienstleister z.B. zum Webhosting unserer Websites und Datenbanken) zur Verarbeitung Ihrer Daten ein. Wenn im Rahmen einer Vereinbarung zur Auftragsverarbeitung an die Auftragsverarbeiter Daten weitergegeben werden, erfolgt dies immer nach Art. 28 DS-GVO. Wir wählen dabei unsere Auftragsverarbeiter sorgfältig aus, kontrollieren diese regelmäßig und haben uns ein Weisungsrecht hinsichtlich der Daten einräumen lassen. Zudem müssen die Auftragsverarbeiter geeignete technische und organisatorische Maßnahmen getroffen haben und die Datenschutzvorschriften gem. BDSG n.F. und DS-GVO einhalten


Datenübermittlung in Drittstaaten
Durch die Verabschiedung der europäischen Datenschutz-Grundverordnung (DS-GVO) wurde eine einheitliche Grundlage für den Datenschutz in Europa geschaffen. Ihre Daten werden daher vorwiegend durch Unternehmen verarbeitet, für die DS-GVO Anwendung findet. Sollte doch die Verarbeitung durch Dienste Dritter außerhalb der Europäischen Union oder des Europäischen Wirtschaftsraums stattfinden, so müssen diese die besonderen Voraussetzungen der Art. 44 ff. DS-GVO erfüllen. Das bedeutet, die Verarbeitung erfolgt aufgrund besonderer Garantien, wie etwa die von der EU-Kommission offiziell anerkannte Feststellung eines der EU entsprechenden Datenschutzniveaus oder der Beachtung offiziell anerkannter spezieller vertraglicher Verpflichtungen, der so genannten „Standardvertragsklauseln“. Soweit wir aufgrund der Unwirksamkeit des sog. „Privacy Shields“, nach Art. 49 Abs. 1 S. 1 lit. a) DSGVO die ausdrückliche Einwilligung in die Datenübermittlung in die USA von Ihnen einholen, weisen wird diesbezüglich auf das Risiko eines geheimen Zugriffs durch US-Behörden und die Nutzung der Daten zu Überwachungszwecken, ggf. ohne Rechtsbehelfsmöglichkeiten für EU-Bürger, hin.


Löschung von Daten und Speicherdauer
Sofern nicht in dieser Datenschutzerklärung ausdrücklich angegeben, werden Ihre personenbezogen Daten gelöscht oder gesperrt, sobald die zur Verarbeitung erteilte Einwilligung von Ihnen widerrufen wird oder der Zweck für die Speicherung entfällt bzw. die Daten für den Zweck nicht mehr erforderlich sind, es sei denn deren weitere Aufbewahrung ist zu Beweiszwecken erforderlich oder dem stehen gesetzliche Aufbewahrungspflichten entgegenstehen. Darunter fallen etwa handelsrechtliche Aufbewahrungspflichten von Geschäftsbriefen nach § 257 Abs. 1 HGB (6 Jahre) sowie steuerrechtliche Aufbewahrungspflichten nach § 147 Abs. 1 AO von Belegen (10 Jahre). Wenn die vorgeschriebene Aufbewahrungsfrist abläuft, erfolgt eine Sperrung oder Löschung Ihrer Daten, es sei denn die Speicherung ist weiterhin für einen Vertragsabschluss oder zur Vertragserfüllung erforderlich.


Bestehen einer automatisierten Entscheidungsfindung
Wir setzen keine automatische Entscheidungsfindung oder ein Profiling ein.


Bereitstellung unserer Website und Erstellung von Logfiles

Wenn Sie unsere Webseite lediglich informatorisch nutzen (also keine Registrierung und auch keine anderweitige Übermittlung von Informationen), erheben wir nur die personenbezogenen Daten, die Ihr Browser an unseren Server übermittelt. Wenn Sie unsere Website betrachten möchten, erheben wir die folgenden Daten:
• IP-Adresse;
• Internet-Service-Provider des Nutzers;
• Datum und Uhrzeit des Abrufs;
• Browsertyp;
• Sprache und Browser-Version;
• Inhalt des Abrufs;
• Zeitzone;
• Zugriffsstatus/HTTP-Statuscode;
• Datenmenge;
• Websites, von denen die Anforderung kommt;
• Betriebssystem.
Eine Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten von Ihnen findet nicht statt.

Diese Daten dienen dem Zweck der nutzerfreundlichen, funktionsfähigen und sicheren Auslieferung unserer Website an Sie mit Funktionen und Inhalten sowie deren Optimierung und statistischen Auswertung.
Rechtsgrundlage hierfür ist unser in den obigen Zwecken auch liegendes berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 S.1 lit. f)  DS-GVO.
Wir speichern aus Sicherheitsgründen diese Daten in Server-Logfiles für die Speicherdauer von  Tagen. Nach Ablauf dieser Frist werden diese automatisch gelöscht, es sei denn wir benötigen deren Aufbewahrung zu Beweiszwecken bei Angriffen auf die Serverinfrastruktur oder anderen Rechtsverletzungen.



Cookies

Wir verwenden sog. Cookies bei Ihrem Besuch unserer Website. Cookies sind kleine Textdateien, die Ihr Internet-Browser auf Ihrem Rechner ablegt und speichert. Wenn Sie unsere Website erneut aufrufen, geben diese Cookies Informationen ab, um Sie automatisch wiederzuerkennen. Zu den Cookies zählen auch die sog. „Nutzer-IDs“, wo Angaben der Nutzer mittels pseudonymisierter Profile gespeichert werden. Wir informieren Sie dazu beim Aufruf unserer Website mittels eines Hinweises auf unsere Datenschutzerklärung über die Verwendung von Cookies zu den zuvor genannten Zwecken und wie Sie dieser widersprechen bzw. deren Speicherung verhindern können („Opt-out“).
Es werden folgende Cookie-Arten unterschieden:
• Notwendige, essentielle Cookies: Essentielle Cookies sind Cookies, die zum Betrieb der Webseite unbedingt erforderlich sind, um bestimmte Funktionen der Webseite wie Logins, Warenkorb oder Nutzereingaben z.B. bzgl. Sprache der Webseite zu speichern.
• Session-Cookies: Session-Cookies werden zum Wiedererkennen mehrfacher Nutzung eines Angebots durch denselben Nutzer (z.B. wenn Sie sich eingeloggt haben zur Feststellung Ihres Login-Status) benötigt. Wenn Sie unsere Seite erneut aufrufen, geben diese Cookies Informationen ab, um Sie automatisch wiederzuerkennen. Die so erlangten Informationen dienen dazu, unsere Angebote zu optimieren und Ihnen einen leichteren Zugang auf unsere Seite zu ermöglichen. Wenn Sie den Browser schließen oder Sie sich ausloggen, werden die Session-Cookies gelöscht.
• Persistente Cookies: Diese Cookies bleiben auch nach dem Schließen des Browsers gespeichert. Sie dienen zur Speicherung des Logins, der Reichweitenmessung und zu Marketingzwecken. Diese werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann. In den Sicherheitseinstellungen Ihres Browsers können Sie die Cookies jederzeit löschen.
• Cookies von Drittanbietern (Third-Party-Cookies insb. von Werbetreibenden): Entsprechend Ihren Wünschen können Sie Ihre Browser-Einstellung konfigurieren und z. B. Die Annahme von Third-Party-Cookies oder allen Cookies ablehnen. Wir weisen Sie jedoch an dieser Stelle darauf hin, dass Sie dann eventuell nicht alle Funktionen dieser Website nutzen können. Lesen Sie Näheres zu diesen Cookies bei den jeweiligen Datenschutzerklärungen zu den Drittanbietern.

Datenkategorien: Nutzerdaten, Cookie, Nutzer-ID (inb. die besuchten Seiten, Geräteinformationen, Zugriffszeiten und IP-Adressen).
Zwecke der Verarbeitung: Die so erlangten Informationen dienen dem Zweck, unsere Webangebote technisch und wirtschaftlich zu optimieren und Ihnen einen leichteren und sicheren Zugang auf unsere Website zu ermöglichen.
Rechtsgrundlagen: Wenn wir Ihre personenbezogenen Daten mit Hilfe von Cookies aufgrund Ihrer Einwilligung verarbeiten („Opt-in“), dann ist Art. 6 Abs. 1 S. 1 lit. a) DSGVO die Rechtsgrundlage. Ansonsten haben wir ein berechtigtes Interesse an der effektiven Funktionalität, Verbesserung und wirtschaftlichen Betrieb der Website, so dass in dem Falle Art. 6 Abs. 1 S. 1 lit. f) DS-GVO Rechtsgrundlage ist. Rechtsgrundlage ist zudem Art. 6 Abs. 1 S. 1 lit. b) DS-GVO, wenn die Cookies zur Vertragsanbahnung z.B. bei Bestellungen gesetzt werden.
Speicherdauer/ Löschung: Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Im Falle der Erfassung der Daten zur Bereitstellung der Website ist dies der Fall, wenn die jeweilige Session beendet ist.Cookies werden ansonsten auf Ihrem Computer gespeichert und von diesem an unsere Seite übermittelt. Daher haben Sie als Nutzer auch die volle Kontrolle über die Verwendung von Cookies. Durch eine Änderung der Einstellungen in Ihrem Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder einschränken. Bereits gespeicherte Cookies können jederzeit gelöscht werden. Dies kann auch automatisiert erfolgen. Werden Cookies für unsere Website deaktiviert, können möglicherweise nicht mehr alle Funktionen der Website vollumfänglich genutzt werden.          
Hier finden Sie Informationen zur L&ouml;schung von Cookies nach Browsern:
Chrome: https://support.google.com/chrome/answer/95647
Safari: https://support.apple.com/de-at/guide/safari/sfri11471/mac
Firefox: https://support.mozilla.org/de/kb/cookies-und-website-daten-in-firefox-loschen
Internet Explorer: https://support.microsoft.com/de-at/help/17442/windows-internet-explorer-delete-manage-cookies
Microsoft Edge: https://support.microsoft.com/de-at/help/4027947/windows-delete-cookies
			
Widerspruch und „Opt-Out“: Das Speichern von Cookies auf Ihrer Festplatte können Sie unabhängig von einer Einwilligung oder gesetzlichen Erlaubnis allgemein verhindern, indem Sie in Ihren Browser-Einstellungen „keine Cookies akzeptieren“ wählen. Dies kann aber eine Funktionseinschränkung unserer Angebote zur Folge haben. Sie können dem Einsatz von Cookies von Drittanbietern zu Werbezwecken über ein sog. „Opt-out“ über diese amerikanische Website (https://optout.aboutads.info) oder diese europäische Website (http://www.youronlinechoices.com/de/praferenzmanagement/) widersprechen.

			





Abwicklung von Verträgen

Wir verarbeiten Bestandsdaten (z.B. Unternehmen, Titel/akademischer Grad, Namen und Adressen sowie Kontaktdaten von Nutzern, E-Mail), Vertragsdaten (z.B. in Anspruch genommene Leistungen, Namen von Kontaktpersonen) und Zahlungsdaten (z.B. Bankverbindung, Zahlungshistorie) zwecks Erfüllung unserer vertraglichen Verpflichtungen (Kenntnis, wer Vertragspartner ist; Begründung, inhaltliche Ausgestaltung und Abwicklung des Vertrags; Überprüfung auf Plausibilität der Daten) und Serviceleistungen (z.B. Kontaktaufnahme des Kundenservice) gem. Art. 6 Abs. 1 S. 1 lit b) DS-GVO. Die in Onlineformularen als verpflichtend gekennzeichneten Eingaben, sind für den Vertragsschluss erforderlich.
Eine Weitergabe dieser Daten an Dritte erfolgt grundsätzlich nicht, außer sie ist zur Verfolgung unserer Ansprüche (z.B. Übergabe an Rechtsanwalt zum Inkasso) oder zur Erfüllung des Vertrags (z.B. Übergabe der Daten an Zahlungsanbieter) erforderlich oder es besteht hierzu besteht eine gesetzliche Verpflichtung gem. Art. 6 Abs. 1 S. 1 lit. c) DS-GVO.
Wir können die von Ihnen angegebenen Daten zudem verarbeiten, um Sie über weitere interessante Produkte aus unserem Portfolio zu informieren oder Ihnen E-Mails mit technischen Informationen zukommen lassen.
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Dies ist für die Bestands- und Vertragsdaten dann der Fall, wenn die Daten für die Durchführung des Vertrages nicht mehr erforderlich sind und keine Ansprüche mehr aus dem Vertrag geltend gemacht werden können, weil diese verjährt sind (Gewährleistung: zwei Jahre / Regelverjährung: drei Jahre). Wir sind aufgrund handels- und steuerrechtlicher Vorgaben verpflichtet, Ihre Adress-, Zahlungs- und Bestelldaten für die Dauer von zehn Jahren zu speichern. Allerdings nehmen wir bei Vertragsbeendigung nach drei Jahren eine Einschränkung der Verarbeitung vor, d. h. Ihre Daten werden nur zur Einhaltung der gesetzlichen Verpflichtungen eingesetzt. Angaben im Nutzerkonto verbleiben bis zu dessen Löschung.



Online Payment-Anbieter

Die Abrechnung erfolgt bei Bezahlung per „Paypal“ über PayPal (Europe) S.àr.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg, Web: paypal.de, https://www.paypal.com/de/webapps/mpp/ua/privacy-full.Die Abrechnung erfolgt bei Bezahlung über „Sofort.com“ über Klarna GmbH, Theresienhöhe 12, 80339 München,  https://www.klarna.com/sofort/datenschutz/.Nachfolgend „Online-Abrechner“ genannt. Die Online-Abrechner erheben, speichern und verarbeiten die Nutzungs- und Abrechnungsdaten von Ihnen zur Ermittlung und zur Abrechnung der von Ihnen in Anspruch genommenen Leistung. Die bei den Online-Abrechnern eingegebenen Daten werden nur durch diese verarbeitet und bei diesen gespeichert. Sofern die Online-Abrechner die Nutzungsentgelte nicht oder nur teilweise einziehen können oder die Online-Abrechner dieses aufgrund einer Beanstandung von Ihnen unterlassen, werden die Nutzungsdaten von den Online-Abrechner an den Verantwortlichen weitergegeben und es erfolgt ggf. eine Sperrung durch den Verantwortlichen. Gleiches gilt auch, wenn z.B. eine Kreditkartengesellschaft eine Transaktion von Ihnen zu Lasten des Verantwortlichen rückabwickelt.

Rechtsgrundlage ist Art. 6 Abs. S. 1 lit. b) DSGVO, da die Verarbeitung zur Erfüllung eines Vertrags durch den Verantwortlichen erforderlich ist. Zudem werden externe Online-Abrechner auf der Grundlage von Art. 6 Abs. 1 S. 1 lit. f) DSGVO aus berechtigten Interessen des Verantwortlichen eingesetzt, um Ihnen möglichst sichere, einfache und vielfältige Zahlungsmöglichkeiten anbieten zu können.
Hinsichtlich der Speicherdauer, Widerrufs-, Auskunfts- und Betroffenenrechte verweisen wir auf die obigen Datenschutzerklärungen der Online-Abrechner.



Nutzung der Blog-Funktionen / Kommentare

Sie können in unserem Blog, der Beiträge zu Themen unserer Website enthält, öffentliche Kommentare abgeben. Sie können ein Pseudonym statt eines Klarnamens verwenden. Ihr Beitrag wird dann unter dem Pseudonym veröffentlicht. Die Angabe der E-Mail-Adresse ist Pflicht, alle sonstigen Informationen sind freiwillig.
Wir speichern bei Ihrer Einstellung eines Kommentars Ihre IP-Adresse mit Datum und Uhrzeit, welche wir nach 90 Tagen löschen. Die Speicherung dient dem berechtigten Interesse der Verteidigung gegen die Inanspruchnahme Dritter bei der Veröffentlichung rechtswidriger oder unwahrer Inhalte durch Sie. Ihre E-Mail-Adresse speichern wir zum Zweck der Kontaktaufnahme falls Dritte Ihre Kommentare juristisch beanstanden sollten.
Rechtsgrundlagen sind Art. 6 Abs. 1 S. 1 lit. b) und f) DS-GVO.
Vor der Veröffentlichung prüfen wir Ihre Kommentare nicht. Im Falle von Beanstandungen Dritter behalten wir uns ein Löschrecht hinsichtlich Ihrer Kommentare vor. Wir geben die Daten nicht an Dritte weiter, es sei denn es ist notwendig zur Verfolgung unserer Ansprüche oder es besteht eine gesetzliche Verpflichtung (Art. 6 Abs. 1 S. 1. lit. c) DS-GVO).
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung bzw. die Durchführung des Vertrages nicht mehr erforderlich sind, weil der Vertrag beendet wurde.




Nutzung unseres Forums

Voraussetzung für die Nutzung des Forums ist die Registrierung über das entsprechende Online-Formular. Sie können das Forum ohne Registrierung lesen und im Falle der Registrierung unter Pseudonym Beiträge und Themen veröffentlichen. Es besteht kein Klarnamenzwang. Nach der Registrierung über das Online-Formular im Forum bekommen Sie eine Bestätigungsemail zur Verifizierung Ihrer Daten zugeschickt, mit der Sie Ihre Registrierung per Mausklick bestätigen können („Double-Opt-In-Verfahren“). Mit der Aktivierung Ihres Accounts durch den den Verantwortlichen, kommt der unentgeltliche Foren-Nutzungsvertrag zustande (Vertragsschluss). Wenn Sie Ihre Registrierung nicht per Mausklick bestätigen, wird Ihre Anmeldung innerhalb von 2160 Stunden bei uns gelöscht.
Wenn wir Ihren Account aktiviert haben, speichern wir bis zu Ihrer Abmeldung zusätzlich zu Ihren Anmeldedaten alle Aktivitäten im Forum, insbesondere Ihre öffentlichen Themen und Beiträge, Ihre Profilangaben, Ihre privaten Nachrichten, Ihre Signatur, Ihre Account-Pinnwand und Ihr Renommee, um das Forum zu betreiben. Wir speichern bei Ihrer Veröffentlichung neuer Themen und Beiträge Ihre IP-Adresse mit Datum und Uhrzeit, welche wir nach 90 Tagen löschen. Die Speicherung dient dem berechtigten Interesse der Verteidigung gegen die Inanspruchnahme Dritter bei der Veröffentlichung rechtswidriger oder unwahrer Inhalte durch Sie. Ihre E-Mail-Adresse und Namen speichern wir zum Zweck der Kontaktaufnahme falls Dritte Ihre Inhalte juristisch beanstanden sollten.
Rechtsgrundlagen sind Art. 6 Abs. 1 S. 1 lit. b) und f) DS-GVO.
Wenn Ihr Foren-Account gelöscht wird, bleiben Ihre Forenbeiträge und -themen weiterhin für alle Leser sichtbar und über Suchmaschinen auffindbar und werden mit „Gast“ gekennzeichnet. Alle sonstigen Daten werden gelöscht. Wenn Sie auch eine Löschung Ihrer Forenbeiträge und -themen wünschen, müssen Sie dies vor der Löschung des Accounts dem Verantwortlichen über die obigen Kontaktdaten mitteilen. Nach der Löschung des Accounts ist eine Zuordnung und Löschung der Beiträge nicht mehr möglich.




Kontaktaufnahme per Kontaktformular / E-Mail / Fax / Post

Bei der Kontaktaufnahme mit uns per Kontaktformular, Fax, Post oder E-Mail werden Ihre Angaben zum Zwecke der Abwicklung der Kontaktanfrage verarbeitet.
Rechtsgrundlage für die Verarbeitung der Daten ist bei Vorliegen einer Einwilligung von Ihnen Art. 6 Abs. 1 S. 1 lit. a) DS-GVO. Rechtsgrundlage für die Verarbeitung der Daten, die im Zuge einer Kontaktanfrage oder E-Mail, eines Briefes oder Faxes übermittelt werden, ist Art. 6 Abs. 1 S. 1 lit. f) DS-GVO. Der Verantwortliche hat ein berechtigtes Interesse an der Verarbeitung und Speicherung der Daten, um Anfragen der Nutzer beantworten zu können, zur Beweissicherung aus Haftungsgründen und um ggf. seiner gesetzlichen Aufbewahrungspflichten bei Geschäftsbriefen nachkommen zu können. Zielt der Kontakt auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 S. 1 lit. b) DS-GVO.
Wir können Ihre Angaben und Kontaktanfrage in unserem Customer-Relationship-Management System ("CRM System") oder einem vergleichbaren System speichern.
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Für die personenbezogenen Daten aus der Eingabemaske des Kontaktformulars und diejenigen, die per E-Mail übersandt wurden, ist dies dann der Fall, wenn die jeweilige Konversation mit Ihnen beendet ist. Beendet ist die Konversation dann, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist. Anfragen von Nutzern, die über einen Account bzw. Vertrag mit uns verfügen, speichern wir bis zum Ablauf von zwei Jahren nach Vertragsbeendigung. Im Fall von gesetzlichen Archivierungspflichten erfolgt die Löschung nach deren Ablauf: Ende handelsrechtlicher (6 Jahre) und steuerrechtlicher (10 Jahre) Aufbewahrungspflicht.
Sie haben jederzeit die Möglichkeit, die Einwilligung nach Art. 6 Abs. 1 S. 1 lit. a) DS-GVO zur Verarbeitung der personenbezogenen Daten zu widerrufen. Nehmen Sie per E-Mail Kontakt mit uns auf, so können Sie der Speicherung der personenbezogenen Daten jederzeit widersprechen.



Kontaktaufnahme per Telefon

Bei der Kontaktaufnahme mit uns per Telefon wird Ihre Telefonnummer zur Bearbeitung der Kontaktanfrage und deren Abwicklung verarbeitet und temporär im RAM / Cache des Telefongerätes / Displays gespeichert bzw. angezeigt. Die Speicherung erfolgt aus Haftungs- und Sicherheitsgründen, um den Beweis des Anrufs führen zu können sowie aus wirtschaftlichen Gründen, um einen Rückruf zu ermöglichen. Im Falle von unberechtigten Werbeanrufen, sperren wir die Rufnummern.
Rechtsgrundlage für die Verarbeitung der Telefonnummer ist Art. 6 Abs. 1 S. 1 lit. f) DS-GVO. Zielt der Kontakt auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 lit. b) DS-GVO.
Der Gerätecache speichert die Anrufe 90 Tage und überschreibt bzw. löscht sukzessiv alte Daten, bei Entsorgung des Gerätes werden alle Daten gelöscht und der Speicher ggf. zerstört. Gesperrte Telefonnummer werden jährlich auf die Notwendigkeit der Sperrung geprüft.
Sie können die Anzeige der Telefonnummer verhindern, indem Sie mit unterdrückter Telefonnummer anrufen.



Newsletter

Unseren Newsletter können Sie mit Ihrer freiwilligen Einwilligung durch Eintragung Ihrer E-Mail-Adresse abonnieren. Nur diese ist Pflicht. Die Angabe weiterer Daten ist freiwillig und dient nur dem Zweck einer persönlichen Ansprache. Wir verwenden dabei zur Anmeldung das sog. „Double-Opt-in-Verfahren“. Nach Ihrer Anmeldung mit Ihrer E-Mail erhalten Sie von uns zur Bestätigung Ihrer Anmeldung eine E-Mail mit einem Link zur Bestätigung. Wenn Sie diesen Bestätigungslink klicken, wird Ihre E-Mail in den Newsletterverteiler aufgenommen und zum Zweck der Übersendung von E-Mails gespeichert. Sollten Sie den Klick auf den Bestätigungslink nicht innerhalb von 24 Stunden durchführen, werden Ihre Anmeldedaten gesperrt und nach 90 Tagen automatisch gelöscht. 
Zudem protokollieren wir Ihre bei der Anmeldung verwendete IP-Adresse sowie das Datum und die Uhrzeit des Double-Opt-ins (Anmeldung und Bestätigung). Zweck dieser Speicherung ist Erfüllung von rechtlichen Anforderungen hinsichtlich des Nachweises Ihrer Anmeldung sowie die Missbrauchsprävention hinsichtlich Ihrer E-Mail.
Im Rahmen Ihrer Einwilligungserklärung werden die Inhalte (z.B. beworbene Produkte/Dienstleistungen, Angebote, Werbung und Themen) des Newsletters konkret beschrieben.
Wir nutzen zum E-Mail-Versand folgenden Versanddienstleister: Zeptomail (Zoho Corporation GmbH Trinkausstr. 7 40213 Düsseldorf,), dessen Datenschutzerklärung finden Sie hier https://www.zoho.com/de/privacy.html. Wir haben mit dem Versanddienstleister eine Vereinbarung zur Auftragsverarbeitung nach Art. 28 DS-GVO abgeschlossen.Beim Versand des Newsletters werten wir Ihr Nutzerverhalten aus. Die Newsletter  beinhalten dafür sogenannte „Web-Beacons“ bzw. „Tracking-Pixel“, die beim Öffnen des Newsletters aufgerufen werden. Für die Auswertungen verknüpfen wir die Web-Beacons mit Ihrer E-Mail-Adresse und einer individuellen ID. Auch im Newsletter erhaltene Links enthalten diese ID. Die Daten werden ausschließlich pseudonymisiert erhoben, die IDs werden also nicht mit Ihren weiteren persönlichen Daten verknüpft, eine direkte Personenbeziehbarkeit wird ausgeschlossen. Mit diesen Daten können wir feststellen, ob und wann Sie den Newsletter geöffnet haben und welche Links im Newsletter geklickt worden sind. Dies dient dem Zweck der Optimierung und statistischen Auswertung unseres Newsletters.
Rechtsgrundlage für den Newsletterversand, Erfolgsmessung und die Speicherung der E-Mail ist Ihre Einwilligung gem. Art. 6 Abs. 1 S. 1 lit. a) DS-GVO i.V.m § 7 Abs. 2 Nr. 3 UWG und für die Protokollierung der Einwilligung Art. 6 Abs. 1 S. 1 lit. f) DS-GVO, da diese unserem berechtigten Interesse der juristischen Beweisbarkeit dient.
Dem Tracking können Sie jederzeit widersprechen, indem Sie den Abmeldelink am Ende des Newsletters klicken. In dem Falle würde allerdings auch der Newsletterempfang beendet. Wenn Sie in Ihrer E-Mail-Software die Anzeige von Bildern deaktivieren, ist ein Tracking ebenfalls nicht möglich. Dies kann allerdings Einschränkungen hinsichtlich der Funktionen des Newsletters haben und enthaltene Bilder werden dann nicht angezeigt.
Sie können Ihre Einwilligung in die Übersendung des Newsletters jederzeit widerrufen. Sie können den Widerruf durch Betätigung des Abmeldelinks am Ende des Newsletters, eine E-Mail oder Mitteilung an unsere obigen Kontaktdaten ausüben. Wir speichern Ihre Daten, solange Sie den Newsletter abonniert haben. Nach der Abmeldung werden Ihre Daten nur noch anonym zu statistischen Zwecken gespeichert.
















YouTube-Videos


Wir haben auf unserer Website YouTube-Videos von youtube.com mittels der embedded-Funktion eingebunden, so dass diese auf unserer Website direkt aufrufbar sind. YouTube gehört zur Google Ireland Limited, Registernr.: 368047, Gordon House, Barrow Street, Dublin 4, Irland.
Datenkategorie und Beschreibung der Datenverarbeitung: Nutzungsdaten (z.B. aufgerufene Webseite, Inhalte und Zugriffszeiten). Wir haben die Videos im sog. „erweiterten Datenschutz-Modus“ eingebunden, ohne dass mit Cookies das Nutzungsverhalten erfasst wird, um die Videowiedergabe zu personalisieren. Stattdessen basieren die Videoempfehlungen auf dem aktuell abgespielten Video. Videos, die im erweiterten Datenschutzmodus in einem eingebetteten Player wiedergegeben werden, wirken sich nicht darauf aus, welche Videos Ihnen auf YouTube empfohlen werden. Beim Start eines Videos (Klick auf das Video) willigen Sie ein, dass YouTube die Information trackt, dass Sie die entsprechende Unterseite bzw. das Video auf unserer Website aufgerufen haben und diese Daten für Werbezecke nutzt.
Zweck der Verarbeitung: Bereitstellung eines nutzerfreundlichen Angebots, Optimierung und Verbesserung unserer Inhalte. 
Rechtsgrundlagen: Haben Sie für Verarbeitung Ihrer personenbezogenen Daten mittels „etracker“ vom Drittanbieter Ihre Einwilligung erteilt („Opt-in“), dann ist Art. 6 Abs. 1 S. 1 lit. a) DS-GVO die Rechtsgrundlage. Rechtsgrundlage ist zudem unser in den obigen Zwecken liegendes berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 S.1 lit. f) DS-GVO. Bei Services, die im Zusammenhang mit einem Vertrag erbracht werden, erfolgt das Tracking und die Analyse des Nutzerhaltens nach Art. 6 Abs. 1 S. 1 lit. b) DS-GVO, um mit den dadurch gewonnen Informationen, optimierte Services zur Erfüllung des Vertragszwecks anbieten zu können.
Datenübermittlung/Empfängerkategorie: Drittanbieter in den USA. Die gewonnenen Daten werden in die USA &uuml;bertragen und dort gespeichert. Dies erfolgt auch ohne Nutzerkonto bei Google. Sollten Sie in Ihren Google-Account eingeloggt sein, kann Google die obigen Daten Ihrem Account zuordnen. Wenn Sie dies nicht w&uuml;nschen, m&uuml;ssen Sie sich in Ihrem Google-Account ausloggen. Google erstellt aus solchen Daten Nutzerprofile und nutzt diese Daten zum Zwecke der Werbung, Marktforschung oder Optimierung seiner Websites.
Speicherdauer: Cookies bis zu 2 Jahre bzw. bis zur Löschung der Cookies durch Sie als Nutzer.
Widerspruch: Sie haben gegen&uuml;ber Google ein Widerspruchsrecht gegen die Bildung von Nutzerprofilen. Bitte richten Sie sich deswegen direkt an Google &uuml;ber die unten genannte Datenschutzerkl&auml;rung. Ein Opt-Out-Widerspruch hinsichtlich der Werbe-Cookies k&ouml;nnen Sie hier in Ihrem Google-Account vornehmen:https://adssettings.google.com/authenticated.
In den Nutzungsbedingungen von YouTube unter https://www.youtube.com/t/terms und in der Datenschutzerkl&auml;rung f&uuml;r Werbung von Google unter https://policies.google.com/technologies/ads finden Sie weitere Informationen zur
Verwendung von Google Cookies und deren Werbetechnologien, Speicherdauer, Anonymisierung, Standortdaten, Funktionsweise und Ihre Rechte. Allgemeine Datenschutzerklärung von Google: https://policies.google.com/privacy.







Datenschutz bei Bewerbungen und im Bewerberverfahren


Bewerbungen, die auf elektronischem Wege oder per Post an den Verantwortlichen gesendet werden, werden zum Zwecke der Abwicklung des Bewerberverfahrens elektronisch oder manuell verarbeitet.
Wir weisen ausdrücklich darauf hin, dass Bewerbungsunterlagen mit „besonderen Kategorien personenbezogener Daten“ nach Art. 9 DS-GVO (z.B. ein Foto, welches Rückschlüsse auf Ihre ethnische Herkunft, Religion oder Ihren Familienstand gibt), mit Ausnahme einer eventuellen Schwerbehinderung, welche Sie aus freier Entscheidung offenlegen möchten, unerwünscht sind. Sie sollen Ihre Bewerbung ohne diese Daten einreichen. Dies hat keine Auswirkungen auf Ihre Bewerberchancen.
Rechtsgrundlagen für die Verarbeitung sind Art. 6 Abs. 1 S.1 lit. b) DS-GVO sowie § 26 BDSG n.F.
Wird nach Abschluss des Bewerberverfahrens, ein Arbeitsverhältnis mit dem Bewerber / der Bewerberin eingegangen, werden die Bewerberdaten unter Beachtung einschlägiger Datenschutzvorschriften gespeichert. Wird Ihnen nach Abschluss des Bewerberverfahrens keine Stelle angeboten, so wird Ihr eingereichtes Bewerbungsschreiben samt Unterlagen 6 Monate nach Versand der Absage gelöscht, um etwaigen Ansprüchen und Nachweispflichten nach AGG genügen zu können.




Rechte der betroffenen Person


Widerspruch oder Widerruf gegen die Verarbeitung Ihrer Daten
Soweit die Verarbeitung auf Ihrer Einwilligung gemäß Art. 6 Abs. 1 S. 1 lit. a), Art. 7 DS-GVO beruht, haben Sie das Recht, die Einwilligung jederzeit zu widerrufen. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung wird dadurch nicht berührt.
Soweit wir die Verarbeitung Ihrer personenbezogenen Daten auf die Interessenabwägung gemäß Art. 6 Abs. 1 S. 1 lit. f) DS-GVO stützen, können Sie Widerspruch gegen die Verarbeitung einlegen. Dies ist der Fall, wenn die Verarbeitung insbesondere nicht zur Erfüllung eines Vertrags mit Ihnen erforderlich ist, was von uns jeweils bei der nachfolgenden Beschreibung der Funktionen dargestellt wird. Bei Ausübung eines solchen Widerspruchs bitten wir um Darlegung der Gründe, weshalb wir Ihre personenbezogenen Daten nicht wie von uns durchgeführt verarbeiten sollten. Im Falle Ihres begründeten Widerspruchs prüfen wir die Sachlage und werden entweder die Datenverarbeitung einstellen bzw. anpassen oder Ihnen unsere zwingenden schutzwürdigen Gründe aufzeigen, aufgrund derer wir die Verarbeitung fortführen.
Sie können der Verarbeitung Ihrer personenbezogenen Daten für Zwecke der Werbung und Datenanalyse jederzeit widersprechen. Das Widerspruchsrecht können Sie kostenfrei ausüben. Über Ihren Werbewiderspruch können Sie uns unter folgenden Kontaktdaten informieren:

akronymAltenbraker Straße 1512053E-Mail-Adresse: info@akronym.org

Recht auf Auskunft
Sie haben ein Recht auf Auskunft über Ihre bei uns gespeicherten persönlichen Daten nach Art. 15 DS-GVO. Dies beinhaltet insbesondere die Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, die Herkunft ihrer Daten, sofern diese nicht direkt bei Ihnen erhoben wurden.
Recht auf Berichtigung
Sie haben ein Recht auf Berichtigung unrichtiger oder auf Vervollständigung richtiger Daten nach Art. 16 DS-GVO.


Recht auf Löschung
Sie haben ein Recht auf Löschung Ihrer bei uns gespeicherten Daten nach Art. 17 DS-GVO, es sei denn gesetzliche oder vertraglichen Aufbewahrungsfristen oder andere gesetzliche Pflichten bzw. Rechte zur weiteren Speicherung stehen dieser entgegen.


Recht auf Einschränkung
Sie haben das Recht, eine Einschränkung bei der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, wenn eine der Voraussetzungen in Art. 18 Abs. 1 lit. a) bis d) DS-GVO erfüllt ist:
• Wenn Sie die Richtigkeit der Sie betreffenden personenbezogenen für eine Dauer bestreiten, die es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;
• die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;
• der Verantwortliche die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigt, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder
• wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DS-GVO eingelegt haben und noch nicht feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren Gründen überwiegen.

Recht auf Datenübertragbarkeit
Sie haben ein Recht auf Datenübertragbarkeit nach Art. 20 DS-GVO, was bedeutet, dass Sie die bei uns über Sie gespeicherten personenbezogenen Daten in einem strukturierten, gängigen und maschinenlesbaren Format erhalten können oder die Übermittlung an einen anderen Verantwortlichen verlangen können.


Recht auf Beschwerde
Sie haben ein Recht auf Beschwerde bei einer Aufsichtsbehörde. In der Regel können Sie sich hierfür an die Aufsichtsbehörde insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes wenden.





Datensicherheit
Um alle personenbezogen Daten, die an uns übermittelt werden, zu schützen und um sicherzustellen, dass die Datenschutzvorschriften von uns, aber auch unseren externen Dienstleistern eingehalten werden, haben wir geeignete technische und organisatorische Sicherheitsmaßnahmen getroffen. Deshalb werden unter anderem alle Daten zwischen Ihrem Browser und unserem Server über eine sichere SSL-Verbindung verschlüsselt übertragen.



Stand: 03.11.2022

Quelle: [Jetzt mehr erfahren](https://www.juraforum.de/datenschutzerklaerung-muster/)
<!--/sse-->