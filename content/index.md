::card-grid
:::card
---
stack: cib:typescript,cib:postgresql,cib:rust,cib:kubernetes
---
Wir bauen [Software für sozialen Wandel]{.text-fuchsia-500} nach euren Vorstellungen und mit unserer Expertise.
:::

:::card
---
stack: cib:python,carbon:sql,cil:terminal
---
Wir entwickeln [menschengerechte Automatisierungen]{.text-fuchsia-500} damit ihr euch wieder auf eure Mission konzentrieren könnt.
:::
::
